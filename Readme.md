<h3>Para instalacion y ejecucion</h3>
```
npm install
```
```
node server.js
```

<h3>links utiles</h3>
<ul>
    <li>
        <p>
            Generacion de json's para pruebas
        </p>
        http://jsonplaceholder.typicode.com/
    </li>
    <li>
        <p>
            Modulo de angular para local storage
        </p>
        https://github.com/grevory/angular-local-storage
    </li>
</ul>

<b>Solucion de error con npm y la instalacion de modulos globales (OSX)</b><br/>
https://docs.npmjs.com/getting-started/fixing-npm-permissions
