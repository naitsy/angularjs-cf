angular.module('CustomDirective',[])
.directive('backImg', function(){
    return function(scope, element, attrs){
        //si no usamos $observe, se accede al valor antes
        //que angular ejecute la funcion de
        //conversion o si el attributo no pertenece a
        //una directiva angular
        // -> attrs.backImg <-

        //con $observe se accede al valor despues que
        //angular ejecutó la funcion de conversion
        attrs.$observe('backImg', function(value){
            element.css({
                'background':'url('+value+')',
                'background-position':'center',
                'background-size':'cover'
            });
        });
    };
})
.controller('AppCtrl', function($scope, $http){
        $http.get('https://api.github.com/users/codigofacilito/repos')
        .success(function(data){
            $scope.repos = data;
        })
        .error(function(err){
            console.log(err);
        });
});

//https://api.github.com/users/codigofacilito/repos
