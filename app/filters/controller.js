var app = angular.module('FilterApp',[]);

app.filter('removeHtml', function(){
    return function(texto){
        return String(texto).replace(/<[^>]+>/gm,'');
    };
});

app.controller('FilterController', function($scope){
    $scope.miHtml ='<p>Hola html</p>';
    $scope.objJSON = {};
    $scope.objJSON.title = 'Titulo';
    $scope.objJSON.body = 'Body';
    $scope.costo = 2;
});
