var app = angular.module('ToDoList',["LocalStorageModule"]);
app.service('ToDoService', function(localStorageService){
    var toDoService = {};
    this.key = 'angular-todo';
    if(localStorageService.get(this.key)){
        this.activities = localStorageService.get(this.key);
    }else{
        this.activities = [];
    }

    this.add = function(newActividad){
        this.activities.push(newActividad);
        this.updateLocalStorage();
    };
    this.updateLocalStorage = function(){
        localStorageService.set(this.key, this.activities);
    };
    this.clean = function(){
        this.activities = [];
        this.updateLocalStorage();
        return this.getAll();
    };
    this.getAll = function(){
        return this.activities;
    };
    this.removeItem = function(item){
        this.activities = this.activities.filter(function(activity){
            return activity !== item;
        });
        return this.getAll();
    };
});

app.controller('ToDoController', function($scope, ToDoService){
    $scope.todo = ToDoService.getAll();
    $scope.newActividad = {};
    $scope.addActividad = function(){
        ToDoService.add($scope.newActividad);
        $scope.newActividad = {};
    };
    $scope.removeItem = function(item){
        $scope.todo = ToDoService.removeItem(item);
    }
    $scope.clean = function(){
        $scope.todo = ToDoService.clean();
    };
});
