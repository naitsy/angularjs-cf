var app = angular.module('ChildApp',[]);
app.run(function($rootScope){
    $rootScope.nombre = 'Padre';
}); //se ejecuta cuando detecta el modulo "ng-app"
app.controller('ParentController', function($scope){
    $scope.nombre = 'Hijo';
    setTimeout(function(){
        $scope.$apply(function(){
            $scope.nombre = 'Javier';
        });
    },3000);
});
app.controller('ChildController', function($scope){

});
