var app = angular.module('HideApp',[]);
app.controller('HideController', function($scope, $http){
    $scope.posts = [];
    $scope.loading = true;
    $http.get('http://jsonplaceholder.typicode.com/posts')
        .success(function(data){
            $scope.posts = data;
            $scope.loading = false;
        })
        .error(function(){
            $scope.loading = false;
        });

});
