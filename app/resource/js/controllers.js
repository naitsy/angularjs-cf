angular.module('FinalApp')
.controller('MainController', function($scope, $resource, PostResource){
    User = $resource('http://jsonplaceholder.typicode.com/users/:id',{id: '@id'});

    $scope.posts = PostResource.query();
    $scope.users = User.query();
    //query() -> GET /post -> un arreglo de post -> EL RESTFul API DEBE devolver un array

    $scope.removePost = function(post){
        PostResource.delete({id: post.id}, function(data){
            console.log(data);
        });
        $scope.posts = $scope.posts.filter(function(el){
            return el.id !== post.id;
        });
    }
})
.controller('PostController', function($scope, $routeParams, PostResource, $location){
    $scope.title = "Editar post";
    $scope.post = PostResource.get({id: $routeParams.id}); // get devuelve UN objeto JSON y no un array
    $scope.savePost = function(){
        PostResource.update({id: $routeParams.id},{data: $scope.post}, function(data){
            console.log(data);
            $location.path('/');
        });
    };
})
.controller('NewPostController', function($scope, PostResource, $location){
    $scope.post = {};
    $scope.title = "Crear post";
    $scope.savePost = function(){
        PostResource.save({data: $scope.post}, function(data){
            console.log(data);
            $location.path('/');
        });
    };
});
