var app = angular.module('ApplyApp',[]);

app.controller('ApplyController', function($scope){
    $scope.nombre = 'Javier';
    $scope.apellido = 'Flores';
    setTimeout(function(){

        $scope.nombre = 'Codigo Facilito';
        $scope.$apply(function(){
            $scope.apellido = 'Codigo Facilito';
        });
    },5000);

});
