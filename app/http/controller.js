var app = angular.module('MyFirstApp',[]);
app.controller('FirstController', function($scope, $http){
    $scope.post = [];
    $scope.newPost = {};
    $http.get('http://jsonplaceholder.typicode.com/posts')
        .success(function(data){
            $scope.posts = data;
            // console.log(data);
        })
        .error(function(){});
    $scope.addPost = function(){
        $http.post('http://jsonplaceholder.typicode.com/posts',{
            title: $scope.newPost.title,
            body: $scope.newPost.body,
            userId: 1
        })
        .success(function(data, status, headers, config){
            $scope.newPost = {};
            $scope.posts.unshift(data);
        })
        .error(function(error, status, headers, config){
            console.log(error);
        });
    }
});
