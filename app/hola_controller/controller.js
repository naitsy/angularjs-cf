var app = angular.module('MyFirstApp',[]);
app.controller('FirstController', ["$scope",function($scope){
    $scope.nombre = 'Javier';
    $scope.nc = {};
    $scope.comentarios = [{
        comentario: 'Buen tutorial',
        usuario: '@codigofacilito'
    },
    {
        comentario: 'Tutorial pedorro',
        usuario: '@naitsy'
    }];
    $scope.agregarComentario = function(){
         $scope.comentarios.push($scope.nc);
    };

}]);
