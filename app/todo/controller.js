var app = angular.module('ToDoList',["LocalStorageModule"]);
app.controller('ToDoController', function($scope, localStorageService){
    if(localStorageService.get('angular-todo')){
        $scope.todo = localStorageService.get('angular-todo');
    }else{
        $scope.todo = [];
    }
    $scope.$watchCollection('todo', function(newValue, oldValue){
        localStorageService.set('angular-todo', $scope.todo);
    });
    $scope.addActividad = function(){
        $scope.todo.push($scope.newActividad);
        $scope.newActividad = {};
    };
    $scope.clean = function(){
        $scope.todo = [];
    };
});
